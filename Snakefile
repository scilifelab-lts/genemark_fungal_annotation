##########################################################################
# This is the Snakefile of a workflow to annotate fungal genomes using   #
# GeneMark-ES                                                            #
#                                                                        #
# 1) Repeat prediction and repeat masking                                #
# 2) Protein alignment                                                   #
# 3) GeneMark gene prediction                                            #
# 4) Functional annotation using InterProScan and BLAST                  #
#                                                                        #
#                                                                        #
# WABI project: A_Rosling_1705                                           #
#                                                                        #
# 1.0: Written by Verena Kutschera, July 2018                            #
#      (verena.kutschera@scilifelab.se)                                  #
# 2.0: Update to include InterProScan, Sept. 2018                        #
# 3.0: Update using only publicly available software, Apr. 2020          #
# 4.0: Update to filter repeat libraries, May 2020                       #
##########################################################################

##########################################################################
########################## SNAKEMAKE PARAMETERS ##########################
##########################################################################

configfile: "config.yaml"

import os
REF_PATH=config["REF_DIR"] + "/" + config["REF_NAME"] + "." + config["REF_EXT"]
REP_REF_DIR=config["REP_DIR"] + "/" + config["REF_NAME"] + "/"
OUT_DIR=config["ANN_DIR"] + "/" + config["REF_NAME"] + "/"
PROT_DIR=os.path.dirname(config["PROT"]) + "/"
PROT_FASTA=os.path.basename(config["PROT"])
PROT_NAME, PROT_EXT=os.path.splitext(PROT_FASTA)

##########################################################################
############################ SNAKEMAKE RULES #############################
##########################################################################

rule all:
    input:
        OUT_DIR + config["REF_NAME"] + ".upper.masked.fasta",
        OUT_DIR + config["REF_NAME"] + ".upper.masked.protein2genome.match.gff.stats",
        OUT_DIR + "genemark.gff.stats",
        OUT_DIR + "genemark.noInternalStop.gff.stats",
        OUT_DIR + "genemark.interpro.blast.gff.stats"

### 1) Repeat modeling and masking

rule ref_upper:
    """Reference assembly file preparation for RepeatModeler and RepeatMasker: change all lowercase bases to uppercase"""
    input:
        ref = REF_PATH
    output:
        ref_upper = REP_REF_DIR + config["REF_NAME"] + ".upper.fasta"
    params:
        dir = REP_REF_DIR
    shell:
        """
        awk '{{ if ($0 !~ />/) {{print toupper($0)}} else {{print $0}} }}' {input.ref} > {output.ref_upper}
        """

rule repeatmodeler:
    """RepeatModeler for de novo repeat prediction from a reference assembly."""
    input:
        ref_upper = rules.ref_upper.output
    output:
        repmo_raw = REP_REF_DIR + "repeatmodeler/" + config["REF_NAME"] + ".consensi.fa.classified"
    params:
        dir = REP_REF_DIR + "repeatmodeler/",
        name = config["REF_NAME"]
    log: REP_REF_DIR + "repeatmodeler/RM.log"
    threads: 4
    shell:
        """
        cd {params.dir}
        module load bioinfo-tools RepeatModeler/1.0.8_RM4.0.7

        # Build repeat database
        BuildDatabase -engine ncbi -name {params.name} {input.ref_upper} &&

        # Run RepeatModeler
        RepeatModeler -engine ncbi -pa {threads} -database {params.name} >& {log} &&

        # copy the output file to a new directory
        cp RM_*.*/consensi.fa.classified {output.repmo_raw}
        """

rule split_uniprot:
    """Split the UniProt/Swissprot protein database into chunks for transposonPSI"""
    input: 
        prot = config["PROT"] # curated proteins from swissprot/uniprot
    output:
        chunks = temp(expand(PROT_DIR + "split_result/" + PROT_NAME + "_chunk{nr}.fa", nr=range(1, 101)))
    params:
        dir = PROT_DIR,
        prot = PROT_FASTA
    conda: "envs/gaas.yaml"
    shell:
        """
        cd {params.dir}
        gaas_fasta_splitter.pl -f {params.prot} --nb_chunks 100 -o tmp &&
        mv tmp/*.fa split_result/ && rm -r tmp/
        """

rule transposonPSI:
    """Identify transposons in the UniProt/Swissprot protein dataset"""
    input:
        chunk = PROT_DIR + "split_result/" + PROT_NAME + "_chunk{nr}.fa"
    output:
        allHits = temp(PROT_DIR + "split_result/" + PROT_NAME + "_chunk{nr}.fa.TPSI.allHits"),
        topHits = temp(PROT_DIR + "split_result/" + PROT_NAME + "_chunk{nr}.fa.TPSI.topHits")
    params:
        dir = PROT_DIR + "split_result/"
    conda: "envs/tePSI.yaml"
    shell:
        """
        cd {params.dir}
        transposonPSI.pl {input.chunk} prot
        """

rule list_tePSI_hits:
    input:
        topHits = expand(PROT_DIR + "split_result/" + PROT_NAME + "_chunk{nr}.fa.TPSI.topHits", nr=range(1, 101))
    output:
        allTopHits = config["PROT"] + ".TPSI.topHits",
        prot_list = config["PROT"] + ".TPSI.topHits.accessions.txt"
    shell:
        """
        cat {input.topHits} > {output.allTopHits} &&
        awk '{{if($0 ~ /^[^\/\/.*]/) print $5}}' {output.allTopHits} | sort -u > {output.prot_list}
        """

rule filter_uniprot_fasta:
    """Remove transposons from the UniProt/Swissprot protein dataset"""
    input:
        prot = config["PROT"],
        prot_list = config["PROT"] + ".TPSI.topHits.accessions.txt"
    output:
        prot_filtered = PROT_DIR + PROT_NAME + ".noTEs.fa"
    params:
        dir = PROT_DIR
    conda: "envs/gaas.yaml"
    shell:
        """
        cd {params.dir}
        gaas_fasta_removeSeqFromIDlist.pl -f {input.prot} -l {input.prot_list} -o {output.prot_filtered}
        """

rule filtered_blast_db:
    """Generate BLAST database from filtered UniProt/Swissprot protein dataset"""
    input: 
        prot_filtered = PROT_DIR + PROT_NAME + ".noTEs.fa"
    output:
        phr = PROT_DIR + PROT_NAME + ".noTEs.fa.phr",
        pin = PROT_DIR + PROT_NAME + ".noTEs.fa.pin",
        psq = PROT_DIR + PROT_NAME + ".noTEs.fa.psq"
    params:
        dir = PROT_DIR
    shell:
        """
        cd {params.dir}
        module load bioinfo-tools blast/2.7.1+
        makeblastdb -in {input.prot_filtered} -dbtype prot
        """

rule blast_repeat_library:
    """Blastx repeat library to filtered Uniprot/Swissprot database"""
    input:
        repmo_raw = REP_REF_DIR + "repeatmodeler/" + config["REF_NAME"] + ".consensi.fa.classified",
        blast_db_idx = rules.filtered_blast_db.output,
        blast_db = PROT_DIR + PROT_NAME + ".noTEs.fa"
    output:
        blast = REP_REF_DIR + "repeatmodeler/"  + config["REF_NAME"] + ".consensi.fa.classified.blastx.out"
    params:
        dir = REP_REF_DIR + "repeatmodeler/"
    threads: 8
    shell:
        """
        cd {params.dir}
        module load bioinfo-tools blast/2.7.1+
        blastx -num_threads {threads} -db {input.blast_db} -query {input.repmo_raw} -out {output.blast}
        """

rule protexcluder:
    """Remove blast hits from repeat library"""
    input:
        repmo_raw = REP_REF_DIR + "repeatmodeler/" + config["REF_NAME"] + ".consensi.fa.classified",
        blast = REP_REF_DIR + "repeatmodeler/"  + config["REF_NAME"] + ".consensi.fa.classified.blastx.out"
    output:
        repmo_fil = REP_REF_DIR + "repeatmodeler/" + config["REF_NAME"] + ".consensi.fa.classifiednoProtFinal"
    params:
        dir = REP_REF_DIR
    conda: "envs/protexcluder.yaml"
    shell:
        """
        cd {params.dir}
        ProtExcluder.pl {input.blast} {input.repmo_raw}
        """

rule repeatmasker:
    """Repeat mask the full genome assembly using raw de novo predicted repeats."""
    input:
        ref_upper = rules.ref_upper.output,
        repmo_fil = rules.protexcluder.output.repmo_fil
    output:
        ref_align = REP_REF_DIR + config["REF_NAME"] + ".upper.fasta.align",
        ref_cat = REP_REF_DIR + config["REF_NAME"] + ".upper.fasta.cat.gz",
        ref_masked = REP_REF_DIR + config["REF_NAME"] + ".upper.fasta.masked",
        ref_out = REP_REF_DIR + config["REF_NAME"] + ".upper.fasta.out",
        ref_tbl = REP_REF_DIR + config["REF_NAME"] + ".upper.fasta.tbl",
        ref_soft = OUT_DIR + config["REF_NAME"] + ".upper.masked.fasta"
    params:
        dir = REP_REF_DIR
    threads: 4
    shell:
        """
        cd {params.dir}
        module load bioinfo-tools RepeatMasker/4.0.7
        RepeatMasker -pa {threads} -a -xsmall -gccalc -dir ./ -lib {input.repmo_fil} {input.ref_upper} &&
        cp {output.ref_masked} {output.ref_soft}
        """


### 2) Protein alignment

rule maker_config_file:
    """Create and edit MAKER configuration files for protein alignment"""
    input:
        ref = OUT_DIR + config["REF_NAME"] + ".upper.masked.fasta", # genome reference, soft masked
        prot = config["PROT"] # curated proteins from swissprot/uniprot
    output:
        bopts = OUT_DIR + "prot_alignment/maker_bopts.ctl",
        exe = OUT_DIR + "prot_alignment/maker_exe.ctl",
        opts = OUT_DIR + "prot_alignment/maker_opts.ctl",
        evm = OUT_DIR + "prot_alignment/maker_evm.ctl",
        tmp_dir = temp(directory(OUT_DIR + "prot_alignment/tmp/"))
    params:
        dir = OUT_DIR + "prot_alignment/"
    shell:
        """
        cd {output.tmp_dir}
        module load bioinfo-tools maker/3.01.1-beta

        # create MAKER config files
        maker -CTL

        # modify the parameter file maker_opts.ctl
        # input files (escaping the slashes in the paths to the input files by using pipes in sed)
        sed -i 's|^genome=|genome={input.ref}|' maker_opts.ctl &&
        sed -i 's|^protein=|protein={input.prot}|' maker_opts.ctl &&

        # other parameters
        sed -i 's/^model_org=all/model_org=/' maker_opts.ctl &&
        sed -i 's|^repeat_protein=/sw/apps/bioinfo/maker/3.01.1-beta/rackham/data/te_proteins.fasta|repeat_protein=|' maker_opts.ctl &&
        sed -i 's/^protein2genome=0/protein2genome=1/' maker_opts.ctl &&
        sed -i 's/^cpus=1/cpus=19/' maker_opts.ctl &&
        
        cd {params.dir}
        mv {output.tmp_dir}maker_*.ctl .
        """

rule maker_align_proteins:
    """Run MAKER to align protein sequences to the soft-masked reference assembly"""
    input:
        bopts = OUT_DIR + "prot_alignment/maker_bopts.ctl",
        exe = OUT_DIR + "prot_alignment/maker_exe.ctl",
        opts = OUT_DIR + "prot_alignment/maker_opts.ctl",
        evm = OUT_DIR + "prot_alignment/maker_evm.ctl"
    output:
        dslog = OUT_DIR + "prot_alignment/" + config["REF_NAME"] + ".upper.masked.maker.output/" + config["REF_NAME"] + ".upper.masked_master_datastore_index.log"
    params:
        dir = OUT_DIR + "prot_alignment/",
        outdir = OUT_DIR + "prot_alignment/" + config["REF_NAME"] + ".upper.masked.maker.output/"
    threads: 19
    shell:
        """
        cd {params.dir}
        module load bioinfo-tools maker/3.01.1-beta
        maker -c {threads} -fix_nucleotides
        """

rule compile_maker_output:
    """Generate a GFF file with genomic locations of protein matches"""
    input:
        dslog = rules.maker_align_proteins.output
    output:
        gff = OUT_DIR + "prot_alignment/" + config["REF_NAME"] + ".upper.masked.maker.output/" + config["REF_NAME"] + ".upper.masked.all.gff"
    params:
        dir = OUT_DIR + "prot_alignment/" + config["REF_NAME"] + ".upper.masked.maker.output/"
    shell:
        """
        cd {params.dir}
        module load bioinfo-tools maker/3.01.1-beta
        gff3_merge -d {input.dslog}
        """

rule extract_protein2genome:
    """Extract protein alignments from MAKER GFF file"""
    input:
        gff = rules.compile_maker_output.output
    output:
        protgff = OUT_DIR + config["REF_NAME"] + ".upper.masked.protein2genome.match.gff"
    params:
        dir = OUT_DIR + "prot_alignment/" + config["REF_NAME"] + ".upper.masked.maker.output/"
    shell:
        """
        cd {params.dir}
        awk -F'\t' '$3 ~ "match" {{print}}' {input.gff} > {output.protgff} &&
        echo "##gff-version 3" | cat - {output.protgff} > temp && mv temp {output.protgff}
        """

rule protein_gff_stats:
    input: 
        gff = rules.extract_protein2genome.output
    output: 
        stats = OUT_DIR + config["REF_NAME"] + ".upper.masked.protein2genome.match.gff.stats"
    params: 
        dir = OUT_DIR
    shell:
        """
        cd {params.dir}
        module load bioinfo-tools GenomeTools/1.5.9
        gt stat {input.gff} > {output.stats}
        """


### 3) GeneMark gene prediction

rule genemark:
    """Run GeneMark with self-prediction, the protein alignment and fungal genome-specific parameters to predict genes in the soft-masked assembly"""
    """The parameter '--soft_mask 1' will lead to internal hardmasking through "X" of all repeats by GeneMark. Masked sequences longer than 5000 bp will be split."""
    input:
        ref = OUT_DIR + config["REF_NAME"] + ".upper.masked.fasta", # genome reference, soft masked
        prots = OUT_DIR + config["REF_NAME"] + ".upper.masked.protein2genome.match.gff", # GFF file from MAKER with protein alignments and gene predictions from the alignments
        stats = OUT_DIR + config["REF_NAME"] + ".upper.masked.protein2genome.match.gff.stats"
    output:
        gtf = OUT_DIR + "genemark.gtf"
    params:
        dir = OUT_DIR,
        key = "$HOME/.gm_key",
        contig_size = config["CONTIG_SIZE"]
    log: 
        log = OUT_DIR + "gmes.log",
        cfg = OUT_DIR + "run.cfg"
    threads: 8
    shell:
        """
        cd {params.dir}
        module load bioinfo-tools GeneMark/4.33-es_Perl5.24.1
        if [ ! -L {params.key} ]; then cp -vf /sw/bioinfo/GeneMark/keyfile/gm_key {params.key}; fi
        /sw/apps/bioinfo/GeneMark/4.33-es/rackham/gmes_petap.pl --ES --fungus --evidence {input.prots} --min_contig {params.contig_size} --soft_mask 1 --cores {threads} --sequence {input.ref}
        """

rule gtf_to_gff:
    input: 
        gtf = rules.genemark.output.gtf
    output: 
        gff = OUT_DIR + "genemark.gff"
    params: 
        dir = OUT_DIR
    shell:
        """
        cd {params.dir}
        module load bioinfo-tools maker/3.01.1-beta
        genemark_gtf2gff3 {input.gtf} > {output.gff}
        """

rule genemark_gff_stats:
    input: 
        gff = rules.gtf_to_gff.output
    output: 
        stats = OUT_DIR + "genemark.gff.stats"
    params: 
        dir = OUT_DIR
    shell:
        """
        cd {params.dir}
        module load bioinfo-tools GenomeTools/1.5.9
        gt stat {input.gff} > {output.stats}
        """


### 4) Functional annotation

rule extract_proteins:
    """Extract protein sequences from reference assembly based on the GeneMark gtf file, with the transcript ID as fasta header"""
    input:
        ref = REF_PATH,
        gtf = rules.genemark.output.gtf
    output:
        fasta = OUT_DIR + "genemark.protein.fasta"
    params:
        dir = OUT_DIR
    shell:
        """
        cd {params.dir}
        module load bioinfo-tools cufflinks/2.2.1-b55bb21
        gffread -y {output.fasta} -O -E -L -F -g {input.ref} {input.gtf}
        """

rule remove_stop_codons_end:
    """Remove '.' from the end of lines that are used as stop codons in gffread, which causes errors when running blast and interproscan to fail"""
    input:
        rules.extract_proteins.output.fasta
    output:
        fasta = OUT_DIR + "genemark.protein.noStopEnd.fasta"
    params:
        dir = OUT_DIR
    shell:
        """
        cd {params.dir}
        sed 's/\.*$//g' {input} > {output.fasta}
        """

rule identify_genes_with_internal_stop:
    """Make a list of genes that contain internal stop codons"""
    input:
        rules.remove_stop_codons_end.output.fasta
    output:
        OUT_DIR + "genemark.protein.genesInternalStop.list"
    params:
        dir = OUT_DIR,
        code_dir = "scripts/python_scripts"
    shell:
        """
        module unload python/3.6.0
        module load biopython/1.68-py3
        python3 {params.code_dir}/identify_genes_internal_stop_codons.py {input} {output}
        """

rule remove_internal_stop_genemark_gff:
    """Remove genes with internal stop codons (coded as '.') from the genemark gff file"""
    input:
        genelist = rules.identify_genes_with_internal_stop.output,
        gff = rules.gtf_to_gff.output
    output:
        fixed_gff = OUT_DIR + "genemark.noInternalStop.gff"
    params:
        dir = OUT_DIR
    shell:
        """
        awk '$0="ID="$0' {input.genelist} | grep -v -f - {input.gff} > {output.fixed_gff}
        """

rule no_internal_stop_gff_stats:
    """Get some simple stats from the gff file"""
    input:
        rules.remove_internal_stop_genemark_gff.output.fixed_gff
    output:
        gff_stats = OUT_DIR + "genemark.noInternalStop.gff.stats"
    params:
        dir = OUT_DIR
    shell:
        """
        cd {params.dir}
        module load bioinfo-tools GenomeTools/1.5.9
        grep -v '# gffread' {input} | gt stat - > {output.gff_stats}
        """

rule remove_internal_stop_protein_fasta:
    """Remove genes with internal stop codons (coded as '.') from the protein fasta file"""
    input:
        genelist = rules.identify_genes_with_internal_stop.output,
        fasta = rules.remove_stop_codons_end.output.fasta
    output:
        tmp = temp(OUT_DIR + "genemark.protein.genesInternalStop.GeneMarkIDs.list"),
        fasta = OUT_DIR + "genemark.protein.noInternalStop.fasta"
    params:
        dir = OUT_DIR,
        code_dir = "scripts/python_scripts"
    shell:
        """
        awk '$0=$0"_t"' {input.genelist} > {output.tmp}
        module unload python/3.6.0
        module load biopython/1.68-py3
        python3 {params.code_dir}/remove_fasta_entries.py {input.fasta} {output.tmp} {output.fasta}
        """

rule interproscan:
    """Search for pathways, families, domains, sites, repeats, structural domains and other sequence features in the InterproScan database"""
    input:
        rules.remove_internal_stop_protein_fasta.output.fasta 
    output:
        tsv_ipr = OUT_DIR + "genemark.protein.noInternalStop.fasta.tsv",
        gff3_ipr = OUT_DIR + "genemark.protein.noInternalStop.fasta.gff3",
        xml_ipr = OUT_DIR + "genemark.protein.noInternalStop.fasta.xml"
    params:
        dir = OUT_DIR
    log: "interproscan.log"
    shell:
        """
        cd {params.dir}
        module load bioinfo-tools InterProScan/5.30-69.0
        interproscan.sh -i {input} -t p -dp -pa --goterms --iprlookup -appl TIGRFAM,SFLD,SUPERFAMILY,PANTHER,Gene3D,Coils,SMART,CDD,PRINTS,SignalP_EUK,Pfam,ProDom,MobiDBLite,PIRSF &> {log}
        """

rule blast_db:
    """Generate BLAST database"""
    input: config["PROT"]
    output:
        phr = config["PROT"] + ".phr",
        pin = config["PROT"] + ".pin",
        psq = config["PROT"] + ".psq"
    params:
        dir = PROT_DIR
    shell:
        """
        cd {params.dir}
        module load bioinfo-tools blast/2.7.1+
        makeblastdb -in {input} -dbtype prot
        """

rule blast:
    """BLASTp of proteins from GeneMark against uniprot database"""
    input:
        fasta = rules.remove_internal_stop_protein_fasta.output.fasta,
        blast_db_idx = rules.blast_db.output,
        blast_db = config["PROT"]
    output:
        blast = OUT_DIR + "genemark.noInternalStop.blastp.out"
    params:
        dir = OUT_DIR
    threads: 8
    shell:
        """
        cd {params.dir}
        module load bioinfo-tools blast/2.7.1+
        blastp -num_threads {threads} -db {input.blast_db} -query {input.fasta} -evalue 10e-5 -outfmt 6 -out {output.blast}
        """

rule update_gff_blast:
    """Add BLAST results to gff file"""
    input:
        gff = rules.gtf_to_gff.output,
        ipr = rules.interproscan.output.tsv_ipr,
        blast = rules.blast.output.blast,
        blast_db = config["PROT"]
    output:
        gff = OUT_DIR + "genemark.interpro.blast.gff"
    params:
        dir = OUT_DIR,
        agatdir = OUT_DIR + "genemark_interpro_blast"
    conda: "envs/agat.yaml"
    shell:
        """
        cd {params.dir}
        agat_sp_manage_functional_annotation.pl -f {input.gff} -b {input.blast} --db {input.blast_db} -i {input.ipr} --output {params.agatdir} &&
        cp {params.agatdir}/genemark.gff {output.gff}
        """

rule ipr_blast_gff_stats:
    """Get some simple stats from the gff file incl. InterProScan and/or BLAST results"""
    input:
        rules.update_gff_blast.output.gff
    output:
        blast_stats = OUT_DIR + "genemark.interpro.blast.gff.stats"
    params:
        dir = OUT_DIR
    shell:
        """
        cd {params.dir}
        module load bioinfo-tools GenomeTools/1.5.9
        grep -v '# gffread' {input} | gt stat - > {output.blast_stats}
        """
