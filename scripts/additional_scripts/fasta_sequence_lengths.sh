#!/bin/bash -l
#SBATCH -A XXX
#SBATCH -p core -n 1
#SBATCH -t 02:00:00
#SBATCH -C usage_mail

# calculate lengths of sequences in a fasta file

dir="" # enter directory containing fasta input file, e.g. "/home/user/annotation_project/GeneMark/assembly_v2.1_idrenamed_short"
fasta="" # enter file name of fasta input file, e.g. "assembly_v2.1_idrenamed_short.fasta"

cd ${dir}

cat ${fasta} | awk '$0 ~ ">" {print c; c=0;printf substr($0,2,100) "\t"; } $0 !~ ">" {c+=length($0);} END { print c; }' > ${fasta}.lengths.txt
