#!/bin/bash -l
#SBATCH -A XXX
#SBATCH -p core -n 1
#SBATCH -t 02:00:00
#SBATCH -J busco

### Usage: loop through protein fasta files (e.g. through /home/user/annotation_project/GeneMark/cclaro_*/genemark.protein.fasta for all C. claroideum annotations)
 
prot=$(basename ${1})
dir=$(dirname ${1})

module load bioinfo-tools BUSCO/3.0.2b R/3.5.0
export AUGUSTUS_CONFIG_PATH=/proj/uppstore2017083/busco/augustus_config
#export R_LIBS_USER=/home/montoliu/R_lib

cd ${dir}
python /sw/apps/bioinfo/BUSCO/3.0.2b/rackham/bin/run_BUSCO.py -i ${prot} -o busco_annotation -l $BUSCO_LINEAGE_SETS/fungi_odb9 -m proteins -sp rhizopus_oryzae -f &&

cd busco_annotation/
python /sw/apps/bioinfo/BUSCO/3.0.2b/rackham/bin/generate_plot.py -wd short_summary_busco_annotation.txt
