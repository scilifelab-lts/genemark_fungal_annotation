#!/bin/bash -l
#SBATCH -A XXX
#SBATCH -p core -n 1
#SBATCH -t 05:00:00

# shorten fasta headers by removing everything after the first whitespace

dir="" # output directory for fasta file with shortened fasta headers
infasta="" # full path incl. file name of input fasta file 
outfasta="" # file name of output fasta file

cd ${dir} 

awk '/^>/ {$0=$1} 1' ${infasta} > ${outfasta}
